

buff_8 db 100 DUP (0) ; 100 Speicherpl�tze (je 1 Byte) reservieren
 ; mit 0 initialisieren

text4 db 0Dh, 0Ah, "Anzahl der Elemente eingeben! n = "
 db "$"
text5 db 0Dh, 0Ah, " Neues Elemente eingeben = " 
 db "$"



 ; Programmteil im Codesegment
 ; Register AX, CX und DX sowie das Flag-Register werden ver�ndert!
 ; Inhalt von Register BX bleibt erhalten! 


vekein8 PROC 

push bx ; SP := SP-2 , [SP] := BL , [SP+1] := BH
vek1: mov ah,9 ; Auswahl der Funktion 9 (Ausgabe einer ZK)
mov dx,OFFSET text4 ; DX := Anfangsadresse der ZK text4
int 21h ; Ausgabe der Zeichenkette text4
call eing_dez ; AL := Hexa-Zahl n (00 � 63h)
cmp al,0 ; wenn Anzahl = 0
jz vek1 ; dann Neueingabe von n

mov cl,al ; CL := AL = n
mov ch,0 ; Z�hlregister (HWT) vorbereiten
mov [bx],al ; n auf erster Pufferspeicherstelle speichern
 
vek2: inc bx ; BX := BX+1
mov ah,9 ; s.o.
mov dx,OFFSET text5 ; s.o.
int 21h ; Systemruf: Ausgabe der Zeichenkette text5 
call eing_dez ; AL := Hexa-Zahl ai (00...63H)
call eing_dez ; AL := Hexa-Zahl n (00 � 63h)
mov [bx],al ; ai abspeichern
loop vek2 ; CX := CX - 1, -> vek2 wenn cx <> 0
pop bx ; BL := [SP] , BH := [SP+1] , SP := SP+2 
; IPL := [SP] , IPH := [SP+1] , SP := SP+2
ret
 
vekein8 ENDP



 ; UP zur Eingabe einer zweistelligen Dezimalzahl und zur Wandlung in eine 
 ; 8bit-Dualzahl (zweistellige Hexa-Zahl: 00h ... 63h) ohne viel Komfort
 ; (Es m�ssen immer zwei Ziffern eingegeben werden, also 03 bei 3!)
 ; Register AX, CX und DX sowie das Flag-Register werden ver�ndert!
 ; Inhalt von Register CX bleibt erhalten! 


eing_dez PROC ;NEAR-Prozedur deklarieren 

push cx ; SP := SP-2 , [SP] := CL , [SP+1] := CH
ein1: mov ah,8 ; AH := NR des Systemaufrufs zur Tastaturabfrage ohne Echo auf dem Bildschirm
int 21h ; AL := Eingabewert (ASCII-Zeichen)
cmp al,30h ; Flags := AL - 30h
jc ein1 ; ---> zu ein1, wenn ASCII-Zeichen kleiner 30H eingegeben wurde

cmp al,3ah ; Flags := AL - 3Ah 
jnc ein1 ; ---> zu ein1, wenn ASCII-Zeichen gr��er 39H
mov dl,al ; DL := AL (ASCII-Zeichen)
mov ah,2
int 21h ; Ausgabe des Zeichens auf Konsole, blieb al erhalten? � ja!
sub al,30h ; AL = AL-30h , ASCII-->BCD-Ziffer (1. Zffer)
mov cl,10 ; Vorbereitung Multiplikation: 1. Ziffer * 1010 
mul cl ; AX:= AL*CL= AL*10, AH = 0, AL:= Ergebnis < 5BH 
mov cl,al ; CL := AL (Zwischenspeicherung)

ein2: mov ah,8 ; AH := NR des Systemaufrufs zur Tastaturabfrage ohne Echo auf dem Bildschirm
int 21h ; AL := Eingabewert (ASCII-Zeichen)
cmp al,30h ; Flags := AL - 30h
jc ein2 ; ---> zu ein2, wenn ASCII-Zeichen kleiner 30H eingegeben wurde
cmp al,3ah ; Flags := AL - 3Ah
jnc ein2 ; ---> zu ein2, wenn ASCII-Zeichen gr��er 39H
mov dl,al ; DL := AL (ASCII-Zeichen)
mov ah,2
int 21h ; Ausgabe des Zeichens auf Konsole, blieb al erhalten? � ja!
sub al,30h ; AL .= AL - 30h , ASCII-->BCD-Ziffer (2. Ziffer)
add al,cl ; AL := AL + CL = (1. Ziffer * 101) + (2. Zifffer* 100) 
pop cx ; CL := [SP] , CH := [SP+1] , SP := SP+2 
ret ; IPL := [SP] , IPH := [SP+1] , SP := SP+2
 
eing_dez ENDP ; Prozedurende deklarieren
