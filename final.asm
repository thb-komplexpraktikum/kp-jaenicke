.MODEL SMALL ;Speichermodel deklarieren
;SMALL = ein Segment CODE und ein Segment DATEN
.STACK 50h ;Stacksegment und Stackgroesse deklarieren
.DATA ;Datensegment deklarieren
b_folge db 7, 15, 20, 25, 30, 35, 40, 45
w_folge dw 3, 1500, 2000, 2500
buff8 db 100 DUP (0)
text1 db 13, 10, "Mittelwert von einer Byte- oder Wortfolge berechnen (b/w)? : "
db "$"
text2 db 13, 10, "Der Mittelwert betraegt: "
db "$"
text3 db " Rest: "
db "$"
text4 db 0Dh, 0Ah, "Anzahl der Elemente eingeben! n = "
db "$"
text5 db 0Dh, 0Ah, " Neues Elemente eingeben = " 
db "$"
.CODE
Startpoint:
start: mov ax,@data ;AX:=@data (Anfangsadresse vom Datensegment)
mov ds,ax ;DS:=AX
eing: mov ah,9 ;Auswahl der Funktion 9 (Ausgabe einer ZK)
mov dx,OFFSET text1 ;dx:= Anfangsadresse der Zeichenkette
int 21h ;Ausgabe der Zeichenkette text1
mov ah,1 ;Sytemruf vorbereiten
int 21h ;Tastatureingabe mit Echo auf dem Bildschirm
cmp al,"b" ;Vergleich mit ASCII-Zeichen von b
je byte_f ;Sprung zu byte_f falls ZF=1 (Bytefolge)
cmp al,"w" ;Vergleich mit ASCII-Zeichen von w 
jne eing ;wenn weder b noch w, dann noch einmal
mov cl,1 ;cl:=1 (Wortfolge)
mov bx,OFFSET w_folge ;bx:= Anfangsadresse der Wortfolge
jmp weiter ;Sprung zu weiter, weil Wortfolge
byte_f:
mov cl,0 ;cl:=0 (Bytefolge)
mov bx,OFFSET buff8 ;bx:= Anfangsadresse der Bytefolge
call vekein8 ;Bytefolge
mov cl,0 ;cl:=0 (Bytefolge) ;erneutes setzen, da c in vekein8 überschrieben wurde
weiter: call mw ;Aufruf des UP Mittelwert (Bsp. 3)
call ausgabe ;Aufruf des UP Ausgabe (Bsp. 8)
mov ax, 4c00h
int 21h ;Rückkehr zu DOS
;jetzt kommt die Auflistung aller verwendeten Unterprogramme 

esu1 PROC ;UP-Anfang deklarieren (Pseudobefehl)
xor ax,ax ;Register leeren
m1: add al, [bx] ;AL := AL + ai = sL, CF=?
adc ah, 0 ;AH := AH + 0 + CF = sH
inc bx ;BX := BX +1
loop m1 ;CX := CX -1, Sprung zu m1, wenn CX <> 0
ret ;Rücksprung ins aufrufende Programm
esu1 ENDP ;UP-Ende deklarieren (Pseudobefehl)

esu2 PROC ;UP-Anfang deklarieren (Pseudobefehl)
push bx ;Stack := BX
push cx ;Stack := CX
xor ax, ax ;AX := AX?? AX = 0 = s
m2: add ax, [bx] ;AX := AX + ai 
inc bx ;BX := BX +1
inc bx ;BX := BX +1
loop m2 ;CX := CX -1, Sprung zu m2, wenn CX <> 0
pop cx ;CX := Stack
pop bx ;BX := Stack
ret ;R?cksprung ins aufrufende Programm
esu2 ENDP ;UP-Ende deklarieren (Pseudobefehl)

mw PROC
push bx ;Stack := BX
push cx ;Stack := CX
cmp cl, 0 ;cl = 0 ? (tb = 0 ?), Flags := cl ? 0
jz byteop ;wenn ja Sprung zu byteop
mov cx, [bx] ;CX := n (wort_var)
inc bx ;BX := BX +1
inc bx ;BX := BX +1
call esu2 ;UP esu2 aufrufen
mov dx, 0 ;Dividend (HWT) vorbereiten
div cx ;AX := (DX,AX)/CX = s/n = aq (dx := Rest)
ende: pop cx ;CX := Stack
pop bx ;BX := Stack
ret ;Rücksprung ins aufrufende Programm

byteop: mov ch, 0 ;CH := 0
mov cl,[bx] ;CL := n (byte_var)
mov dl, cl ;DL := n
inc bx ;BX := BX +1
call esu1 ;UP esu1 aufrufen
;mov ah, 0 ;Dividend (HWT) vorbereiten ;entfernt, da ganz ax gebraucht wird
div dl ;AL := AX/DL = s/n = aq (ah := Rest)
jmp ende ;Sprung zur Marke ende
mw ENDP

;Unterprogramm zur Ausgabe des Mittelwertes mw und des Restes einer 
;Folge von Byte- bzw. Wort-Elementen auf dem Bildschirm
ausgabe PROC
cmp cl, 0 ;Mittelwert einer Byte- oder Wortfolge ausgeben?
je mw_byte ;Sprung zu mw_byte, wenn Ausgabe mw einer Bytefolge
push dx ;Rest retten
push ax ;MW retten
mov ah,9 ;Funktion 9 auswählen
mov dx, OFFSET text2 ;DX:= Anfangsadresse der ZK text2
int 21h ;Ausgabe der Zeichenkette text2
pop ax ;AX := Mittelwert (Wortfolge)
call ausg_dez ;Ausgabe des Mittelwertes (Bsp. 6)
mov ah,9
mov dx, OFFSET text3
int 21h ;Ausgabe der Zeichenkette text3
pop ax ;AX := Rest (Wortfolge)
call ausg_dez ;Ausgabe des Restes (Bsp. 6)
ret
mw_byte:
push ax ;MW und Rest retten
mov ah,9
mov dx, OFFSET text2
int 21h ;Ausgabe der Zeichenkette text2
pop ax ;AL := Mittelwert, AH := Rest (Bytefolge)
push ax ;und noch weiter im Stack retten
mov ah,0 ;Rest l?schen
call ausg_dez ;Ausgabe des Mittelwertes (Bsp. 6)
mov ah,9
mov dx, OFFSET text3
int 21h ;Ausgabe der Zeichenkette text3
pop ax ;AL := Mittelwert, AH := Rest (Bytefolge)
mov al,ah ;AL := Rest
mov ah,0 ;AH := 0
call ausg_dez ;Ausgabe des Restes (Bsp. 6)
ret
ausgabe ENDP 

ausg_dez PROC 
mov bx,10 ;Divisor nach BX laden
xor cx,cx ;Schleifenzähler auf Null
rech: xor dx,dx;Dividend (HWT) vorbereiten
div bx ;AX:=DXAX/BX (AX durch 10 teilen), DX:=Rest
push dx ;Rest der Division auf den Stack
inc cx ;Anzahl der Ziffern Zählen
or ax,ax ;Ist Ergebnis schon Null?
jnz rech ;Nein, dann noch einmal
mov ah,2 ;Funktionsnummer für Zeichenausgabe
aus: pop dx ;Ziffer wieder vom Stack holen
add dl,"0" ;AL := AL+30H (in ASCII umwandeln)
int 21h ;und danach ausgeben
loop aus ;Schleife wiederholen entsprechend der Anzahl der Ziffern
ret
ausg_dez ENDP 

;Methoden aus bsp10
;Doppelte Abfrage der Eingabe entfernt (absichtlich eingebauter Fehler, der dazu diente zu gucken ob die Studenten es auch mal austesten)

;Eingabe von Bytefolge
vekein8 PROC 
push bx ; SP := SP-2 , [SP] := BL , [SP+1] := BH
vek1: mov ah,9 ; Auswahl der Funktion 9 (Ausgabe einer ZK)
mov dx,OFFSET text4 ; DX := Anfangsadresse der ZK text4
int 21h ; Ausgabe der Zeichenkette text4
call eing_dez ; AL := Hexa-Zahl n (00  63h)
cmp al,0 ; wenn Anzahl = 0
jz vek1 ; dann Neueingabe von n
mov cl,al ; CL := AL = n
mov ch,0 ; Zählregister (HWT) vorbereiten
mov [bx],al ; n auf erster Pufferspeicherstelle speichern
vek2: inc bx ; BX := BX+1
mov ah,9 ; s.o.
mov dx,OFFSET text5 ; s.o.
int 21h ; Systemruf: Ausgabe der Zeichenkette text5 
call eing_dez ; AL := Hexa-Zahl ai (00...63H)
mov [bx],al ; ai abspeichern
loop vek2 ; CX := CX - 1, -> vek2 wenn cx <> 0
pop bx ; BL := [SP] , BH := [SP+1] , SP := SP+2 
; IPL := [SP] , IPH := [SP+1] , SP := SP+2
ret
vekein8 ENDP

;Eingabe von Dezimalzahl
eing_dez PROC
push cx ; SP := SP-2 , [SP] := CL , [SP+1] := CH
ein1: mov ah,8 ; AH := NR des Systemaufrufs zur Tastaturabfrage ohne Echo auf dem Bildschirm
int 21h ; AL := Eingabewert (ASCII-Zeichen)
cmp al,30h ; Flags := AL - 30h
jc ein1 ; ---> zu ein1, wenn ASCII-Zeichen kleiner 30H eingegeben wurde
cmp al,3ah ; Flags := AL - 3Ah 
jnc ein1 ; ---> zu ein1, wenn ASCII-Zeichen größer 39H
mov dl,al ; DL := AL (ASCII-Zeichen)
mov ah,2
int 21h ; Ausgabe des Zeichens auf Konsole, blieb al erhalten?  ja!
sub al,30h ; AL = AL-30h , ASCII-->BCD-Ziffer (1. Zffer)
mov cl,10 ;Berechnung Erstes Segment
mul cl ; AX:= AL*CL= AL*10, AH = 0, AL:= Ergebnis < 5BH 
mov cl,al ; CL := AL (Zwischenspeicherung)

ein2: mov ah,8 ; AH := NR des Systemaufrufs zur Tastaturabfrage ohne Echo auf dem Bildschirm
int 21h ; AL := Eingabewert (ASCII-Zeichen)
cmp al,30h ; Flags := AL - 30h
jc ein2 ; ---> zu ein2, wenn ASCII-Zeichen kleiner 30H eingegeben wurde
cmp al,3ah ; Flags := AL - 3Ah
jnc ein2 ; ---> zu ein2, wenn ASCII-Zeichen größer 39H
mov dl,al ; DL := AL (ASCII-Zeichen)
mov ah,2
int 21h ; Ausgabe des Zeichens auf Konsole, blieb al erhalten?  ja!
sub al,30h ; AL .= AL - 30h , ASCII-->BCD-Ziffer (2. Ziffer)
add al,cl ; AL := AL + CL = (1. Ziffer * 101) + (2. Zifffer* 100) 
pop cx ; CL := [SP] , CH := [SP+1] , SP := SP+2 
ret ; IPL := [SP] , IPH := [SP+1] , SP := SP+2
eing_dez ENDP ; Prozedurende deklarieren

END Startpoint ;Ende des zu assemblierenden Quelltextes
